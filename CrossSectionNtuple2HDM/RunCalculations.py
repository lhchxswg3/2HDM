#!/usr/bin/env python

"""
This program does all the hard work of setting up the
parameter scan grids (which are more easily configured 
in python than in C++). It then executes the C++ program,
giving it just one set of parameters (e.g. mH, sba, tb, sqrts,...)
to handle. This allows the parameter grid to be computed
separately from the execution of 2hdmc, SusHi, etc. and 
should allow for this all to be broken into very small 
pieces for execution on a batch system.
"""


import os
import numpy
import sys
import getopt
import itertools
import re
import copy
import time
import math


###

global _Statistics
_Statistics = {}
_Statistics["GridTotal"] = 0
_Statistics["GridProc"] = 0

# Regular Expression search/match patterns for SusHi filenames
_re_sqrts = re.compile('.*sqrts=([0-9]+)TeV.*')
_re_mh = re.compile('.*mh=(.*?)_.*')
_re_mH = re.compile('.*mH=(.*?)_.*')
_re_mA = re.compile('.*mA=(.*?)_.*')
_re_tb = re.compile('.*tb=(.*?)_.*')
_re_sba = re.compile('.*sba=(.*?)_.*')
_re_higgs = re.compile('.*higgs=(.*)\.sushi')
_re_type = re.compile('.*type=(.*?)_.*')

###########################################################
# The usage() function, for printing usage instructions
# on the command line
###########################################################

def usage():
    print ' \
RunCalculations.py <options> \n \
\n \
where <options> is one and only one of . . . \n\
\n \
   -h/--help           Print help information (this info) \n \
   -s/--sushi <N>      Run SusHi on the 2HDM parameter grid \n \
                       Bunch <N> grid points together in each job. \n \
   -c/--calc  <N>      Using Sushi output and 2HDMC, calculate the results \n \
                       and generate an output ROOT file \n \
                       Process <N> SusHi output files per job. \n\
   -t/--test           Test, but do not actually run, the code - do not try to \n \
                       write input and output files, etc. \n \
'
    pass

def MakeSushiInputFileName(mh=0.0,mH=0.0,mA=0.0,mC=0.0,tb=0.0,sba=0.0,m122=0.0,sqrts=0,thdm=0,higgs=0):
    # update: tb 1->2 sba 5->6
    filename =  "input_" + \
        "mh=%.1f_" % (mh) + \
        "mH=%.1f_" % (mH) + \
        "mA=%.1f_" % (mA) + \
        "mC=%.1f_" % (mC) + \
        "tb=%.2f_" % (tb) + \
        "sba=%.6f_" % (sba) + \
        "m122=%.6f_" % (m122) + \
        "sqrts=%dTeV_" % (sqrts) + \
        "type=%d_" % (thdm) + \
        "higgs=%d" % (higgs) + \
        ".sushi"

    return filename

def MakeSushiInputFile(input_filename=""):

    sqrts = int(_re_sqrts.match(input_filename).group(1))
    higgs = int(_re_higgs.match(input_filename).group(1))
    mh    = float(_re_mh.match(input_filename).group(1))
    mH    = float(_re_mH.match(input_filename).group(1))
    mA    = float(_re_mA.match(input_filename).group(1))
    tb    = float(_re_tb.match(input_filename).group(1))
    sba   = float(_re_sba.match(input_filename).group(1))
    type  = int(_re_type.match(input_filename).group(1))

    alpha = math.atan(tb)-math.asin(sba)

    # generate input file text from template
    input_file = _sushi_template
    input_file = input_file.replace("7000.d0","%d000.d0" % (sqrts))
    input_file = input_file.replace("  2   0         # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)", \
                                    "  2   %d         # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)" % (higgs))
    input_file = input_file.replace(" 25   125.00000d0"," 25   %.1fd0       # Higgs mass h" % (mh))
    input_file = input_file.replace(" 35   150.00000d0"," 35   %.1fd0       # Higgs mass H" % (mH))
    input_file = input_file.replace(" 36   300.00000d0"," 36   %.1fd0       # Pseudoscalar" % (mA))
    input_file = input_file.replace("  3   5.d0","  3   %.2fd0" % (tb)) # tb 1->2f
    input_file = input_file.replace(" -5.0e-01"," %.7fd0               # mixing in Higgs sector" % (alpha)) # sba 5->6f alpha 5->7
    input_file = input_file.replace("  1			# (1=I,2=II,3=III,4=IV)", "  %d                     # (1=I,2=II,3=III,4=IV)" % (type))
    
    sushi_input_file = open(input_filename,"w")
    sushi_input_file.write(input_file)
    sushi_input_file.close()
    


def ProcessGrid(grid, gridkey, calc_m122=False):
    TotalGridPoints = len(grid)

    _Statistics["GridTotal"] += TotalGridPoints
    
    print "INFO: there are a total of " + str(TotalGridPoints) + " points to process."
    print "INFO: bunching " + str(_nPointsPerSushiJob) + " points together per job."

    GridPoint = 0
    while GridPoint < TotalGridPoints:
        ListOfSusHiInputFiles = []

        # construct the Condor Job ID and the output directory for this job
        job_id = time.time()
        output_directory = "job_files/%s/"%(job_id)


        for GridPointInJob in xrange(_nPointsPerSushiJob):
            # construct the name of the input files and submit them
            # to Condor using the SushiWrapper.sh as the executable
            # mh
            mh      = grid[GridPoint][gridkey["mh"]]
            mH      = grid[GridPoint][gridkey["mH"]]
            mA      = grid[GridPoint][gridkey["mA"]]
            mC      = grid[GridPoint][gridkey["mC"]]
            tb      = grid[GridPoint][gridkey["tb"]]
            sba     = grid[GridPoint][gridkey["sba"]]
            sqrts   = grid[GridPoint][gridkey["sqrts"]]
            thdm    = grid[GridPoint][gridkey["thdm"]]
            m122    = 0.0
            if calc_m122:
                m122 = mH**2 * tb / (1.0 + tb**2)
                pass

            PointIsProcessed = False
            for HiggsProduced in [0,1,2]:

                # create the filename for input/output
                input_filename = MakeSushiInputFileName(mh,mH,mA,mC,tb,sba,m122,sqrts,thdm,higgs=HiggsProduced)
                output_filename = input_filename.replace('input_','output_')

                # existence check - do not process if output exists
                if not (output_filename in _sushiFiles):
                    PointIsProcessed = True
                    ListOfSusHiInputFiles.append(input_filename)
                    pass

                pass

            if PointIsProcessed:
                _Statistics["GridProc"] += 1
                pass

            
            GridPoint += 1

            if GridPoint >= TotalGridPoints:
                break

            pass
        
        # construct the batch job for submission
        if len(ListOfSusHiInputFiles) > 0:
            # make the directory for the job, write the input files, etc.
            if not os.path.exists(output_directory):
                os.makedirs(output_directory)
            for input_filename in ListOfSusHiInputFiles:
                MakeSushiInputFile(input_filename=output_directory+"/"+input_filename)
                pass
            
            command_to_run = "./user_submit -c ./SushiWrapper.sh -a \'"
            command_to_run += "-d %s/ " % (output_directory)
            for input_file in ListOfSusHiInputFiles:
                command_to_run += input_file + " "
                pass
            command_to_run += "\' -n 1000"

            command_to_run += " -o %s"%(output_directory)

            # print command_to_run
            if _doTest:
                print command_to_run
            else:
                os.system(command_to_run)
                pass

            pass
        

        PointsLeftToRun = TotalGridPoints - GridPoint
        print "INFO: there are " + str(PointsLeftToRun) + " grid points left to run."

        pass
    pass



###########################################################
# Main Analysis Code
###########################################################

global _doSushi
global _nPointsPerSushiJob
global _doCalc
global _doTest

_doSushi = False
_nPointsPerSushiJob = 1
_doCalc  = False
_doTest = False

debug = False
batch = True

try:
    opts, args = getopt.getopt(sys.argv[1:], "hs:c:t", ["help", "sushi=", "calc=","test"])
except getopt.GetoptError:
    usage()
    sys.exit(2)
for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit()
    elif opt in ("-s", "--sushi"):
        _doSushi = True
        _nPointsPerSushiJob = int(arg)
    elif opt in ("-c", "--calc"):
        _doCalc = True
        _nPointsPerSushiJob = int(arg)
        pass
    elif opt in ("-t", "--test"):
        _doTest = True
        pass
    pass

if _doSushi and _doCalc:
    # Can't have it both ways!
    usage()
    sys.exit(2)

# Check for critical components

if not os.path.exists("sushi"):
    print "ERROR: The SusHi executable is not present in the current directory."
    print "       Please copy it here or link it here before running."
    sys.exit(-1)
    



################################### DEFINE the 2HDM GRID ######################################

# Values of some parameters that change less frequently
mh_array = [125.0]
m122 = 0.0

# generate the major mH grid
#mH_array =  numpy.linspace(130,150,40,endpoint=False).tolist()
#mH_array += numpy.linspace(150,300,75,endpoint=False).tolist()
#mH_array += numpy.linspace(165,200,7,endpoint=False).tolist()
#mH_array += numpy.linspace(300,350,10,endpoint=False).tolist()
#mH_array += numpy.linspace(350,400,5,endpoint=False).tolist()
#mH_array += numpy.linspace(400,1020,31,endpoint=False).tolist()
#mH_array += numpy.linspace(450,1000,11,endpoint=False).tolist()
# no need of many mass pts, according to groupd demand
#mH_array = [
#200, 220, 240, 260, 280,
#300, 320, 340, 350, 360, 380,
#400, 420, 440, 450, 460, 480,
#500, 520, 540, 550, 560, 580,
#600, 650, 700, 750, 800,
#850, 900, 950, 1000,
#1100, 1200, 1300, 1400, 1500
#]
#mH_array = sorted(mH_array)

# mass 37
# let's do it round by round due to limited computing source
# main grid
mH_array = [ 220, 240, 260, 300, 340, 350, 400, 500, 800, 1000] # 10, mass-grid-I
#mH_array = [ 600, 700, 900, 1100, 1200, 1300, 1400, 1500 ] # 8 mass-grid-II
# finner grid
#mH_array = [ 200, 280, 320, 360, 380, 420, 440, 450, 460, 480 ] # 10 mass-grid-III
#mH_array = [ 520, 540, 550, 560, 580, 650, 750, 850, 950 ] # 9 mass-grid-IV

# specially for mA/H 200~350 mC=350
#mH_array = [ 200, 220, 240, 260, 280, 300, 320, 340, 350 ] # 10, mass-grid-I

# additional for AZh 29-10-2014
# only mA = 300GeV
#mH_array = [300]

# special for HZZ->4l adding mass 140 150 160 170 180 190 [20150521]
mH_array = [ 140,150,160,170,180,190 ]


print "======================== mH Grid ========================"
print mH_array

# generate the major tan(beta) grid
#I suggest that we make a finer tanbeta grid for type-II
#between tanb= 1.0 and 8.0, in steps of 0.2 in tanbeta.
#tb_array =  numpy.linspace(0.1,1.0,9,endpoint=False).tolist()

# new tb enhanced 1.0-0.8
#tb_array =  numpy.linspace(0.1,1.0,45,endpoint=False).tolist()
#tb_array += numpy.linspace(1.0,8.0,35,endpoint=False).tolist()
#tb_array += numpy.linspace(8.0,10.0,2,endpoint=False).tolist()
#tb_array += numpy.linspace(10.0,25.0,3,endpoint=False).tolist()
#tb_array += [25,30,35,40,45,50]

# generated by above but write down explicitly avoiding machine precision issue
# require sushi file tanb=0.00 two digit "%.2f"
tb_array = [0.1, 0.12, 0.14, 0.16, 0.18, 0.2, 0.22, 0.24, 0.26, 0.28, 0.30, 0.32, 0.34, 0.36, 0.38, 0.4, 0.42, 0.44, 0.46, 0.48, 0.5, 0.52, 0.54, 0.56, 0.58, 0.6, 0.62, 0.64, 0.66, 0.68, 0.7, 0.72, 0.74, 0.76, 0.78, 0.8, 0.82, 0.84, 0.86, 0.88, 0.9, 0.92, 0.94, 0.96, 0.98, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.2, 5.4, 5.6, 5.8, 6.0, 6.2, 6.4, 6.6, 6.8, 7.0, 7.2, 7.4, 7.6, 7.8, 8.0, 9.0, 10.0, 15.0, 20.0, 25, 30, 35, 40, 45, 50]
# tb_array 91

# additional for AZh 29-10-2014
# tb_array =  numpy.linspace(0.1,1.0,90,endpoint=False).tolist()
#tb_array = [
#0.11,  0.13,  0.15,  0.17,  0.19,  0.21,  0.23,  0.25,  0.27,
#0.29,  0.31,  0.33,  0.35,  0.37,  0.39,  0.41,  0.43,  0.45,
#0.47,  0.49,  0.51,  0.53,  0.55,  0.57,  0.59,  0.61,  0.63,
#0.65,  0.67,  0.69,  0.71,  0.73,  0.75,  0.77,  0.79,  0.81,
#0.83,  0.85,  0.87,  0.89,  0.91,  0.93,  0.95,  0.97,  0.99
#]

# special for HZZ->4l adding mass 140 150 160 170 180 190 [20150521]
#tb_array = [ 0.5, 0.52, 0.54, 0.56, 0.58, 0.6, 0.62, 0.64, 0.66, 0.68, 0.7, 0.72, 0.74, 0.76, 0.78, 0.8, 0.82, 0.84, 0.86, 0.88, 0.9, 0.92, 0.94, 0.96, 0.98, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.2, 5.4, 5.6, 5.8, 6.0, 6.2, 6.4, 6.6, 6.8, 7.0, 7.2, 7.4, 7.6, 7.8, 8.0, 9.0, 10.0 ]


print "======================== tb Grid ========================"
print tb_array

# generate the major sin(b-a) grid
# no change on sba
#sba_array = [-1.0, -0.99875, -0.99499, -0.98869, -0.97980, -0.96825, -0.95394, -0.93675, -0.91652, -0.9, -0.89303, -0.86603, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.86603, 0.89303, 0.9, 0.91652, 0.93675, 0.95394, 0.96825, 0.97980, 0.98869, 0.99499, 0.99875, 1.0]
# add finer steps in cba of 0.02 from 0.0-0.2
#for i in range(1,20):
#    sba_array.append( math.sin(math.acos(i*0.02)) )
#    sba_array.append( math.sin(-math.acos(i*0.02)) )
#    pass
#sba_array = sorted(sba_array)
# generated by above, write explicitly down
# require sushi file precision "%.6f"
sba_array = [-1.0, -0.999799979995999, -0.9991996797437437, -0.99875, -0.9981983770774224, -0.996794863550169, -0.99499, -0.99498743710662, -0.9927738916792685, -0.9901515035589251, -0.98869, -0.9871170143402452, -0.983666610188635, -0.9798, -0.9797958971132713, -0.9754998718605759, -0.9707728879609278, -0.96825, -0.9656086163658649, -0.96, -0.95394, -0.9539392014169457, -0.9474175425861608, -0.9404254356406998, -0.93675, -0.9329523031752481, -0.9249864863877743, -0.91652, -0.9, -0.89303, -0.86603, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.86603, 0.89303, 0.9, 0.91652, 0.9249864863877743, 0.9329523031752481, 0.93675, 0.9404254356406998, 0.9474175425861608, 0.9539392014169457, 0.95394, 0.96, 0.9656086163658649, 0.96825, 0.9707728879609278, 0.9754998718605759, 0.9797958971132713, 0.9798, 0.983666610188635, 0.9871170143402452, 0.98869, 0.9901515035589251, 0.9927738916792685, 0.99498743710662, 0.99499, 0.996794863550169, 0.9981983770774224, 0.99875, 0.9991996797437437, 0.999799979995999, 1.0]
# sba_array 78

# special for HZZ->4l adding mass 140 150 160 170 180 190 [20150521]
# cba=-0.1 0.1
#sba_array = [ -0.994987, 0.994987 ]

print "======================== sin(b-a) Grid ========================"
print sba_array

print "...corresponding to cos(b-a) = ..."
for sba in sba_array:
    print math.cos(math.asin(sba))

# generate the major sqrt(s) grid
#sqrts_array = [7,8] # 7,8
sqrts_array = [13] # 7,8
print "======================== sqrt{s} Grid ========================"
print sqrts_array

# generate the major 2HDM Type-X grid
type_array = [1,2] # [1,2]
print "======================== 2HDM Type-X Grid ========================"
print type_array


################################### END OF DEFINE the 2HDM GRID ######################################


global _sushiFiles
_sushiFiles = set()

# Load the list of existing SusHi input/output files into an array in memory,
# for speed later

# Create the directory needed for output
if not os.path.exists("sushi_files_7TeV_type1"):
    os.makedirs("sushi_files_7TeV_type1")
    pass
if not os.path.exists("sushi_files_7TeV_type2"):
    os.makedirs("sushi_files_7TeV_type2")
    pass
if not os.path.exists("sushi_files_8TeV_type1"):
    os.makedirs("sushi_files_8TeV_type1")
    pass
if not os.path.exists("sushi_files_8TeV_type2"):
    os.makedirs("sushi_files_8TeV_type2")
    pass

# 13TeV for sushi
if not os.path.exists("sushi_files_13TeV_type1"):
    os.makedirs("sushi_files_13TeV_type1")
    pass
if not os.path.exists("sushi_files_13TeV_type2"):
    os.makedirs("sushi_files_13TeV_type2")
    pass

print "INFO: loading existing list of SusHi files for speed processing later..."
_sushiFiles = set(os.listdir('sushi_files_7TeV_type1/')) | set(os.listdir('sushi_files_7TeV_type2/')) | set(os.listdir('sushi_files_8TeV_type1/')) | set(os.listdir('sushi_files_8TeV_type2/')) | set(os.listdir('sushi_files_13TeV_type1/')) | set(os.listdir('sushi_files_13TeV_type2/'))
print "Done"

# Load the SusHi input template file into memory, to avoid 
# heavy i/o reading the template for each job
print "Loading SusHi template file."
global _sushi_template
sushi_template_file = open('in2HDM.in','r')
_sushi_template = sushi_template_file.read()
sushi_template_file.close()
print "Done"


if _doSushi:
    print "INFO: executing SusHi for each point in the GRID to generate"
    print "      files for the computation stage (-c)"

    
    # generate the n-dimensions grid of all unique scan points from the above
    #superlist = [mh_array,mH_array,tb_array,sba_array,sqrts_array,type_array]
    #gridkey  = {"mh":0, "mH": 1, "tb": 2, "sba": 3, "sqrts": 4, "thdm": 5, "mA": 1, "mC": 1}
    #grid = list(itertools.product(*superlist))

    #ProcessGrid(grid,gridkey,calc_m122=False)

    #
    # sba narrow scan for m122 != 0
    # add more tb points
    #
    #tb_array += [25,30,35,40,45,50] # move all grid definition in one place above
    #sba_array += [-0.99999, -0.9999, -0.999, -0.99, -0.98, -0.95, 0.95, 0.98, 0.99, 0.999, 0.9999, 0.99999]
    superlist = [mh_array,mH_array,tb_array,sba_array,sqrts_array,type_array]
    gridkey  = {"mh": 0, "mH": 1, "mC": 1, "tb": 2, "sba": 3, "sqrts": 4, "thdm": 5, "mA": 1}
    grid = list(itertools.product(*superlist))


    #superlist = [[125.0],[500.0],[5.0],[0.95],[7,8],[1,2]]
    #grid = list(itertools.product(*superlist))

    ProcessGrid(grid,gridkey,calc_m122=True)

    print "INFO: Execution Statistics:"
    print "      Total Grid Points Considered: %d" % (_Statistics["GridTotal"])
    print "      Total Grid Points Processed:  %d" % (_Statistics["GridProc"])

    pass
    
elif _doCalc:

    # instead, take the sample of existing .sushi output files and
    # use those to generate the ROOT files needed by analysts

    # Load only the output files for 7TeV and Higgs=0. Let the code compute the rest.
    # It will cycle over 8TeV and Higgs=0,1,2
    
    calcFiles = []

    for file in _sushiFiles:
        if file.find("higgs=0") != -1 and file.find("sqrts=7") != -1 and \
               file.find("m122=0.000000") == -1 and file.find("sba=0.0000") == -1:
            calcFiles.append(file)
        if file.find("higgs=0") != -1 and file.find("sqrts=13") != -1 and \
               file.find("m122=0.000000") == -1 and file.find("sba=0.0000") == -1:
            calcFiles.append(file)
            pass
        pass

    # Only process to ROOT those SusHi output files in the calcFiles list
    TotalSushiFiles = len(calcFiles)

    
    print "INFO: there are a total of " + str(TotalSushiFiles) + " SusHi files to process."
    print "INFO: bunching " + str(_nPointsPerSushiJob) + " files together per job."

    SushiFile = 0

    if not os.path.exists("root_files"):
        os.makedirs("root_files")

    BatchJob = 0

    while SushiFile < TotalSushiFiles:
        ListOfSusHiFiles = []

        # construct the Condor Job ID and the output directory for this job
        job_id = time.time()
        output_directory = "job_files/%s/"%(job_id)


        for SushiFileInJob in xrange(_nPointsPerSushiJob):

            theFile = calcFiles[SushiFile]

            ListOfSusHiFiles.append(calcFiles[SushiFile])
                        
            SushiFile += 1

            if SushiFile >= TotalSushiFiles:
                break

            pass

        # construct the batch job for submission
        if len(ListOfSusHiFiles) > 0:
            if not os.path.exists(output_directory):
                os.makedirs(output_directory)
                pass
            
            command_to_run = "./user_submit -c ./CrossSection2HDM.exe -a '"
            if 13 in sqrts_array:
              command_to_run = "./user_submit -c ./CrossSection2HDM13TeV.exe -a '"
            for input_file in ListOfSusHiFiles:
                input_dir = "sushi_files"
                if input_file.find("7TeV") != -1:
                    input_dir = input_dir + "_7TeV"
                elif input_file.find("8TeV") != -1:
                    input_dir = input_dir + "_8TeV"
                elif input_file.find("13TeV") != -1:
                    input_dir = input_dir + "_13TeV"
                    pass
                if input_file.find("type=1") != -1:
                    input_dir = input_dir + "_type1/"
                elif input_file.find("type=2") != -1:
                    input_dir = input_dir + "_type2/"
                    pass
                command_to_run += " -s " + input_dir + input_file
                pass
            command_to_run += " root_files/thdm_%08d.root'" % (BatchJob)
            command_to_run += " -o %s" % (output_directory)
            command_to_run += " -n 1000"
            
            # print command_to_run
            if _doTest:
                print command_to_run
            else:
                os.system( command_to_run ) # sometimes arguments is too long
                pass

            BatchJob += 1

            pass
        

        FilesLeftToRun = TotalSushiFiles - SushiFile
        print "INFO: there are " + str(FilesLeftToRun) + " sushi files left to run."

        pass
    pass
    
        



